﻿using UnityEngine;

namespace Game
{
	[CreateAssetMenu(fileName = "GameContent", menuName = "ScriptableObjects/GameContent", order = 1)]
	public class GameContent : ScriptableObject
	{
		public PlayerController playerPrefab;
		public EnemyController enemyPrefab;
		public int initialHealth;
		public float reloadTime;
		public int damageAmount;
	}
}