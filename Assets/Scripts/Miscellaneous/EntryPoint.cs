﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class EntryPoint : MonoBehaviour
	{
		[SerializeField] private GameContent gameContent;
		[SerializeField] private SceneDependencies sceneDependencies;

		private BattleController battleController;
		private KeyboardInputController inputController;
		private PlayerInputController playerInputController;
		private CameraController cameraController;
		private BattleModel battleModel;
		private bool inited;

		private void Awake()
		{
			Init();
		}

		private void OnDestroy()
		{
			Deinit();
		}

		private void Update()
		{
			if (!inited)
			{
				return;
			}

			inputController.Update();
			battleController.Update();
			sceneDependencies.battleWindow.ExternalUpdate();
		}

		private void Init()
		{
			var inputModel = new InputModel();
			battleModel = GetBattleModel(gameContent.initialHealth);

			var spawnController = new SpawnController(gameContent.playerPrefab, gameContent.enemyPrefab);
			var lifeCycleController = new LifeCycleController(
				battleModel,
				spawnController,
				sceneDependencies.playerSpawnPoint,
				sceneDependencies.enemySpawnPoints,
				sceneDependencies.enemyWayPoints,
				gameContent.reloadTime,
				gameContent.damageAmount);

			var lootController = new LootController(battleModel);
			var rewardController = new RewardController(battleModel);
			var battleWindowModel = new BattleWindowModel(battleModel, sceneDependencies.activeCamera);

			inputController = new KeyboardInputController(inputModel);
			battleController = new BattleController(battleModel, lifeCycleController, lootController, rewardController);
			battleController.Init();

			playerInputController = new PlayerInputController(battleModel.Player.Controller.MovementController, inputModel);
			playerInputController.Init();

			cameraController = new CameraController(sceneDependencies.activeCamera, battleModel.Player);
			cameraController.Init();

			sceneDependencies.lootView.Init(battleModel);
			sceneDependencies.battleWindow.Init(battleWindowModel);

			battleModel.BattleFinished += OnBattleFinished;

			inited = true;
		}

		private void Deinit()
		{
			battleController.Deinit();
			playerInputController.Deinit();
			cameraController.Deinit();

			sceneDependencies.lootView.Deinit();
			sceneDependencies.battleWindow.Deinit();

			battleModel.BattleFinished -= OnBattleFinished;

			inited = false;
		}

		private async void OnBattleFinished()
		{
			Deinit();

			await System.Threading.Tasks.Task.Delay(1000);

			Init();
		}

		// In a read project that data gets extracted from meta-gameplay.
		private BattleModel GetBattleModel(int initialHealth)
		{
			var player = new PlayerModel();

			var enemies = new List<EnemyModel>()
		{
			new EnemyModel(initialHealth),
			new EnemyModel(initialHealth),
			new EnemyModel(initialHealth)
		};

			return new BattleModel(player, enemies);
		}
	}
}