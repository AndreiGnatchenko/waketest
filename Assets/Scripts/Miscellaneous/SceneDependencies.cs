﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class SceneDependencies : MonoBehaviour
	{
		public Transform playerSpawnPoint;
		public List<Transform> enemySpawnPoints;
		public List<Transform> enemyWayPoints;
		public Camera activeCamera;
		public LootView lootView;
		public BattleWindow battleWindow;
	}
}