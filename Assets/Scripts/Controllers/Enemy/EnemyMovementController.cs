﻿using UnityEngine;

namespace Game
{
	public class EnemyMovementController : MonoBehaviour
	{
		[SerializeField] private CharacterController characterController;
		[SerializeField] private Transform anchor;

		private const float SqrMagnitudeThreshold = 0.2f;

		private EnemyModel model;
		private Vector3 localMovement;

		public void Init(EnemyModel model)
		{
			this.model = model;
		}

		public void Deinit()
		{
		}

		public void ExternalUpdate()
		{
			UpdatePosition();
			UpdateRotation();
		}

		public void Move(Vector3 desiredMovement)
		{
			localMovement = desiredMovement;
		}

		public void Aim(Vector3 desiredDirection)
		{
			model.Direction.Value = desiredDirection;
		}

		private void UpdatePosition()
		{
			var velocity = anchor.TransformDirection(localMovement);
			characterController.SimpleMove(velocity);

			model.Position.Value = anchor.position;
		}

		private void UpdateRotation()
		{
			var direction = model.Direction.Value;

			var clampedDirection = new Vector3(direction.x, 0.0f, direction.z);

			if (Vector3.SqrMagnitude(clampedDirection) > SqrMagnitudeThreshold)
			{
				anchor.rotation = Quaternion.LookRotation(clampedDirection);
			}
		}
	}
}
