﻿using UnityEngine;

namespace Game
{
	public class EnemyHealthController
	{
		private EnemyModel enemy;

		public EnemyHealthController(EnemyModel enemy)
		{
			this.enemy = enemy;
		}

		public void Damage(int damage)
		{
			if (enemy.IsDead)
			{
				return;
			}

			var validDamage = -Mathf.Min(enemy.Health.Value, Mathf.Abs(damage));

			enemy.Health.Value += validDamage;

			if (enemy.IsDead)
			{
				enemy.PublishDead(enemy);
			}
		}
	}
}
