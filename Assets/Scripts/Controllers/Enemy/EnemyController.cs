﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class EnemyController : MonoBehaviour
	{
		[SerializeField] private EnemyMovementController movementController;

		public EnemyHealthController HealthController { get; private set; }
		private EnemyLogicController logicController;

		private EnemyModel enemy;

		public void Init(EnemyModel enemy, List<Transform> wayPoints)
		{
			this.enemy = enemy;

			enemy.Health.Changed += OnHealthChanged;

			logicController = new EnemyLogicController(enemy, wayPoints, movementController);
			HealthController = new EnemyHealthController(enemy);

			movementController.Init(enemy);
		}

		public void Deinit()
		{
			enemy.Health.Changed -= OnHealthChanged;

			movementController.Deinit();
		}

		public void ExternalUpdate()
		{
			if (enemy.IsDead)
			{
				return;
			}

			movementController.ExternalUpdate();
			logicController.ExternalUpdate();
		}

		private void OnHealthChanged(int health)
		{
			if (enemy.IsDead)
			{
				gameObject.SetActive(false);
			}
		}
	}
}
