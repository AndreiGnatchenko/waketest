﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class EnemyLogicController
	{
		private const float SqrCiticalDistance = 1f;

		private EnemyModel enemyModel;
		private EnemyMovementController movementController;
		private List<Transform> wayPoints;

		private int currentIndex;

		public EnemyLogicController(EnemyModel enemyModel, List<Transform> wayPoints, EnemyMovementController movementController)
		{
			this.enemyModel = enemyModel;
			this.movementController = movementController;
			this.wayPoints = wayPoints;

			currentIndex = Random.Range(0, wayPoints.Count);
		}

		public void ExternalUpdate()
		{
			var currentPoint = wayPoints[currentIndex];

			var direction = currentPoint.position - enemyModel.Position.Value;

			movementController.Move(Vector3.forward);
			movementController.Aim(direction.normalized);

			if (IsWayPointReached(currentPoint))
			{
				currentIndex = (currentIndex + 1) % wayPoints.Count;
			}
		}

		private bool IsWayPointReached(Transform wayPoint)
		{
			var sqrDistance = Vector3.SqrMagnitude(wayPoint.position - enemyModel.Position.Value);

			return sqrDistance <= SqrCiticalDistance;
		}
	}
}
