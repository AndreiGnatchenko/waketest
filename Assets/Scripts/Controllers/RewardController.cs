﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class RewardController
	{
		private const float SqrDistanceThreshold = 1.0f;

		private List<LootModel> lootToTake = new List<LootModel>();

		private BattleModel battleModel;

		public RewardController(BattleModel battleModel)
		{
			this.battleModel = battleModel;
		}

		public void Update()
		{
			lootToTake.Clear();

			foreach (var loot in battleModel.Loot)
			{
				if (CanTake(loot))
				{
					lootToTake.Add(loot);
				}
			}

			foreach (var loot in lootToTake)
			{
				TakeLoot(loot);

				battleModel.Loot.Remove(loot);

				battleModel.PublishLootRemoved(loot.Id);
			}
		}

		private void TakeLoot(LootModel loot)
		{
			battleModel.Player.CoinsCount.Value += 1;
		}

		private bool CanTake(LootModel loot)
		{
			var playerPosition = battleModel.Player.Position.Value;

			var lootPosition = loot.Postion;

			var sqrDistance = Vector3.SqrMagnitude(lootPosition - playerPosition);

			return sqrDistance <= SqrDistanceThreshold;
		}
	}
}