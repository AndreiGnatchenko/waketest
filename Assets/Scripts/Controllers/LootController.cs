﻿using UnityEngine;

namespace Game
{
	public class LootController
	{
		private BattleModel battleModel;

		private int lootCounter;

		public LootController(BattleModel battleModel)
		{
			this.battleModel = battleModel;
		}

		public void Init()
		{
			battleModel.Enemies.ForEach(item => item.Dead += OnEnemyDead);
		}

		public void Deinit()
		{
			battleModel.Enemies.ForEach(item => item.Dead -= OnEnemyDead);
		}

		private void OnEnemyDead(EnemyModel enemy)
		{
			SpawnLoot(enemy.Position.Value);
		}

		private void SpawnLoot(Vector3 position)
		{
			var lootModel = new LootModel(lootCounter++, position);

			battleModel.Loot.Add(lootModel);

			battleModel.PublishLootAdded(lootModel);
		}
	}
}