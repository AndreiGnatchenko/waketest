﻿using UnityEngine;

namespace Game
{
	public class CameraController
	{
		private Camera activeCamera;
		private PlayerModel player;

		public CameraController(Camera activeCamera, PlayerModel player)
		{
			this.activeCamera = activeCamera;
			this.player = player;
		}

		public void Init()
		{
			player.Position.Changed += OnPositionChanged;
		}

		public void Deinit()
		{
			player.Position.Changed -= OnPositionChanged;
		}

		private void OnPositionChanged(Vector3 position)
		{
			var activePosition = activeCamera.transform.position;

			activeCamera.transform.position = new Vector3(position.x, activePosition.y, position.z);
		}
	}
}