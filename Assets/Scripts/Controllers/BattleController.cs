﻿namespace Game
{
	public class BattleController
	{
		private BattleModel battleModel;
		private LifeCycleController lifeCycleController;
		private LootController lootController;
		private RewardController rewardController;

		public BattleController(BattleModel battleModel, LifeCycleController lifeCycleController, LootController lootController, RewardController rewardController)
		{
			this.battleModel = battleModel;
			this.lifeCycleController = lifeCycleController;
			this.lootController = lootController;
			this.rewardController = rewardController;
		}

		public void Init()
		{
			lifeCycleController.Init();
			lootController.Init();
		}

		public void Deinit()
		{
			lifeCycleController.Deinit();
			lootController.Deinit();
		}

		public void Update()
		{
			lifeCycleController.Update();
			rewardController.Update();

			if (IsBattleFinished())
			{
				battleModel.PublishBattleFinished();
			}
		}

		private bool IsBattleFinished()
		{
			var allEnemiesAreDead = battleModel.Enemies.Find(item => !item.IsDead) == null;

			var allCoinsAreTaken = battleModel.Loot.Count == 0;

			return allEnemiesAreDead && allCoinsAreTaken;
		}
	}
}