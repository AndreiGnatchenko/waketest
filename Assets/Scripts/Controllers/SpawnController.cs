﻿using UnityEngine;

namespace Game
{
	public class SpawnController
	{
		private PlayerController playerPrefab;
		private EnemyController enemyPrefab;

		public SpawnController(PlayerController playerPrefab, EnemyController enemyPrefab)
		{
			this.playerPrefab = playerPrefab;
			this.enemyPrefab = enemyPrefab;
		}

		public PlayerController CreatePlayer(Transform spawnPoint)
		{
			var instance = GameObject.Instantiate(playerPrefab);

			instance.transform.position = spawnPoint.position;
			instance.transform.rotation = spawnPoint.rotation;

			return instance;
		}

		public void DestroyPlayer(PlayerController player)
		{
			GameObject.Destroy(player.gameObject);
		}

		public EnemyController CreateEnemy(Transform spawnPoint)
		{
			var instance = GameObject.Instantiate(enemyPrefab);

			instance.transform.position = spawnPoint.position;
			instance.transform.rotation = spawnPoint.rotation;

			return instance;
		}

		public void DestroyEnemy(EnemyController enemy)
		{
			GameObject.Destroy(enemy.gameObject);
		}
	}
}
