﻿using UnityEngine;

namespace Game
{
	public class KeyboardInputController
	{
		private InputModel model;

		public KeyboardInputController(InputModel model)
		{
			this.model = model;
		}

		public void Update()
		{
			HandleMovement();
		}

		private void HandleMovement()
		{
			var selectedDirection = InputDirection.None;

			if (Input.GetKey(KeyCode.W))
			{
				selectedDirection = InputDirection.Forward;
			}
			else if (Input.GetKey(KeyCode.S))
			{
				selectedDirection = InputDirection.Backward;
			}
			else if (Input.GetKey(KeyCode.D))
			{
				selectedDirection = InputDirection.Right;
			}
			else if (Input.GetKey(KeyCode.A))
			{
				selectedDirection = InputDirection.Left;
			}

			if (model.Movement.Value != selectedDirection)
			{
				model.Movement.Value = selectedDirection;
			}
		}
	}
}