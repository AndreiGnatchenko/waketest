﻿using UnityEngine;

namespace Game
{
	public class PlayerMovementController : MonoBehaviour
	{
		[SerializeField] private CharacterController characterController;
		[SerializeField] private Transform anchor;

		private const float Speed = 2.0f;
		private const float SqrMagnitudeThreshold = 0.2f;

		private PlayerModel model;
		private Vector3 localMovement;

		public void Init(PlayerModel model)
		{
			this.model = model;
		}

		public void Deinit()
		{
		}

		public void ExternalUpdate()
		{
			UpdatePosition();
			UpdateRotation();
		}

		public void Move(Vector3 desiredMovement)
		{
			localMovement = desiredMovement;

			model.Active.Value = localMovement.sqrMagnitude >= SqrMagnitudeThreshold;
		}

		public void Aim(float desiredRotation)
		{
			model.Direction.Value = new Vector3(0.0f, desiredRotation, 0.0f);
		}

		private void UpdatePosition()
		{
			var velocity = anchor.TransformDirection(localMovement);
			characterController.SimpleMove(velocity * Speed);

			model.Position.Value = anchor.position;
		}

		private void UpdateRotation()
		{
			anchor.rotation = Quaternion.Euler(model.Direction.Value);
		}
	}
}
