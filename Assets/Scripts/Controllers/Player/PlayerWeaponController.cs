﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerWeaponController
	{
		private PlayerModel player;
		private PlayerMovementController movementController;
		private List<EnemyModel> enemies;

		private float reloadTime;
		private int damageAmount;

		public PlayerWeaponController(PlayerModel player, PlayerMovementController movementController, List<EnemyModel> enemies, float reloadTime, int damageAmount)
		{
			this.player = player;
			this.movementController = movementController;
			this.enemies = enemies;
			this.reloadTime = reloadTime;
			this.damageAmount = damageAmount;
		}

		public void Update()
		{
			if (!player.Active.Value)
			{
				var nearestEnemy = GetNearestEnemy();

				if (nearestEnemy == null)
				{
					return;
				}

				var enemyDirection = nearestEnemy.Position.Value - player.Position.Value;

				var angle = -Vector3.SignedAngle(enemyDirection, Vector3.forward, Vector3.up);

				movementController.Aim(angle);

				if (CanShoot())
				{
					Shoot(nearestEnemy);
				}
			}
		}

		private void Shoot(EnemyModel enemy)
		{
			enemy.Controller.HealthController.Damage(damageAmount);

			player.LastFireTime.Value = Support.Support.GetTime();
		}

		private bool CanShoot()
		{
			var prevTime = player.LastFireTime.Value;

			var currTime = Support.Support.GetTime();

			return prevTime + reloadTime <= currTime;
		}

		private EnemyModel GetNearestEnemy()
		{
			EnemyModel nearestEnemy = null;
			var nearestDistance = float.MaxValue;

			foreach (var enemy in enemies)
			{
				if (enemy.IsDead)
				{
					continue;
				}

				var currentDistance = Vector3.SqrMagnitude(enemy.Position.Value - player.Position.Value);

				if (nearestDistance > currentDistance)
				{
					nearestDistance = currentDistance;
					nearestEnemy = enemy;
				}
			}

			return nearestEnemy;
		}
	}
}
