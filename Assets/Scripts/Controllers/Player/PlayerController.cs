﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField] private PlayerMovementController movementController;

		public PlayerMovementController MovementController => movementController;

		private PlayerWeaponController weaponController;

		public void Init(PlayerModel player, List<EnemyModel> enemies, float reloadTime, int damageAmount)
		{
			movementController.Init(player);

			weaponController = new PlayerWeaponController(player, movementController, enemies, reloadTime, damageAmount);
		}

		public void Deinit()
		{
			movementController.Deinit();
		}

		public void ExternalUpdate()
		{
			movementController.ExternalUpdate();
			weaponController.Update();
		}
	}
}
