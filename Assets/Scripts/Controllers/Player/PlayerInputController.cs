﻿using UnityEngine;

namespace Game
{
	public class PlayerInputController
	{
		private PlayerMovementController movementController;
		private InputModel inputModel;

		public PlayerInputController(PlayerMovementController movementController, InputModel inputModel)
		{
			this.movementController = movementController;
			this.inputModel = inputModel;
		}

		public void Init()
		{
			inputModel.Movement.Changed += OnMovementChanged;
		}

		public void Deinit()
		{
			inputModel.Movement.Changed -= OnMovementChanged;
		}

		private void OnMovementChanged(InputDirection input)
		{
			var direction = GetDirectionFromInput(input);

			movementController.Move(direction);
		}

		private Vector3 GetDirectionFromInput(InputDirection input)
		{
			switch (input)
			{
				case InputDirection.Left: return Vector3.left;
				case InputDirection.Right: return Vector3.right;
				case InputDirection.Forward: return Vector3.forward;
				case InputDirection.Backward: return Vector3.back;

				default: return Vector3.zero;
			}
		}
	}
}
