﻿using System.Collections.Generic;
using UnityEngine;
using Support;

namespace Game
{
	public class LifeCycleController
	{
		private BattleModel battleModel;
		private SpawnController spawnController;
		private Transform playerSpawnPoint;
		private List<Transform> enemySpawnPoints;
		private List<Transform> enemyWayPoints;
		private float reloadTime;
		private int damageAmount;

		public LifeCycleController(BattleModel battleModel, SpawnController spawnController, Transform playerSpawnPoint, List<Transform> enemySpawnPoints, List<Transform> enemyWayPoints, float reloadTime, int damageAmount)
		{
			this.battleModel = battleModel;
			this.spawnController = spawnController;
			this.playerSpawnPoint = playerSpawnPoint;
			this.enemySpawnPoints = enemySpawnPoints;
			this.enemyWayPoints = enemyWayPoints;
			this.reloadTime = reloadTime;
			this.damageAmount = damageAmount;
		}

		public void Init()
		{
			var playerController = spawnController.CreatePlayer(playerSpawnPoint);
			playerController.Init(battleModel.Player, battleModel.Enemies, reloadTime, damageAmount);
			battleModel.Player.Controller = playerController;

			var randomSpawnPonits = enemySpawnPoints.Shuffle();

			for (var i = 0; i < battleModel.Enemies.Count; i++)
			{
				var enemy = battleModel.Enemies[i];
				var point = randomSpawnPonits[i % randomSpawnPonits.Count];

				enemy.Controller = spawnController.CreateEnemy(point);
				enemy.Controller.Init(enemy, enemyWayPoints);
			}
		}

		public void Deinit()
		{
			battleModel.Player.Controller.Deinit();
			spawnController.DestroyPlayer(battleModel.Player.Controller);

			foreach (var enemy in battleModel.Enemies)
			{
				enemy.Controller.Deinit();
				spawnController.DestroyEnemy(enemy.Controller);
			}
		}

		public void Update()
		{
			battleModel.Player.Controller.ExternalUpdate();
			battleModel.Enemies.ForEach(item => item.Controller.ExternalUpdate());
		}
	}
}
