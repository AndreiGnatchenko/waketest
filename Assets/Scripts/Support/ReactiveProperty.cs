﻿using System;

namespace Support
{
    public class ReactiveProperty<T>
    {
        public event Action<T> Changed;
        public event Action<T, T> ChangedDiff;

        public T Value
        {
            get
            {
                return internalValue;
            }

            set
            {
                var prevValue = internalValue;

                internalValue = value;

                Changed?.Invoke(internalValue);
                ChangedDiff?.Invoke(prevValue, internalValue);
            }
        }

        private T internalValue;
    }
}