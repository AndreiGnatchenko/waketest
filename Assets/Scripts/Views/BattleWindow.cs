﻿using UnityEngine;

namespace Game
{
	public class BattleWindow : MonoBehaviour
	{
		[SerializeField] private EnemyHealthViewController enemyHealthViewController;
		[SerializeField] private PlayerScoreView playerScoreView;

		public void Init(BattleWindowModel model)
		{
			enemyHealthViewController.Init(model.BattleModel.Enemies, model.BattleCamera);
			playerScoreView.Init(model.BattleModel.Player);
		}

		public void Deinit()
		{
			enemyHealthViewController.Deinit();
			playerScoreView.Deinit();
		}

		public void ExternalUpdate()
		{
			enemyHealthViewController.ExternalUpdate();
		}
	}
}