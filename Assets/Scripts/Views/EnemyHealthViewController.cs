﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class EnemyHealthViewController : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private EnemyHealthView viewPrefab;

		private List<EnemyHealthView> views = new List<EnemyHealthView>();

		public void Init(List<EnemyModel> enemies, Camera battleCamera)
		{
			foreach (var enemy in enemies)
			{
				var view = Instantiate(viewPrefab, viewAnchor);

				view.Init(enemy, battleCamera);

				views.Add(view);
			}
		}

		public void Deinit()
		{
			foreach (var view in views)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			views.Clear();
		}

		public void ExternalUpdate()
		{
			views.ForEach(view => view.ExternalUpdate());
		}
	}
}