﻿using UnityEngine;

namespace Game
{
	public class CoinView : MonoBehaviour
	{
		public LootModel Model { get; private set; }

		public void Init(LootModel model)
		{
			Model = model;

			transform.position = model.Postion;
		}

		public void Deinit()
		{

		}
	}
}