﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class EnemyHealthView : MonoBehaviour
	{
		[SerializeField] private Image bar;

		private const float PixelShift = 25f;

		private EnemyModel model;
		private Camera battleCamera;

		public void Init(EnemyModel model, Camera battleCamera)
		{
			this.model = model;
			this.battleCamera = battleCamera;

			model.Health.Changed += OnHealthChanged;
			OnHealthChanged(model.Health.Value);
		}

		public void Deinit()
		{
			model.Health.Changed -= OnHealthChanged;
		}

		public void ExternalUpdate()
		{
			var screenPosition = battleCamera.WorldToScreenPoint(model.Position.Value) + Vector3.up * PixelShift;

			transform.position = screenPosition;
		}

		private void OnHealthChanged(int currentHealth)
		{
			var ratio = (float)currentHealth / model.InitialHealth;

			bar.fillAmount = ratio;

			if (model.IsDead)
			{
				gameObject.SetActive(false);
			}
		}
	}
}