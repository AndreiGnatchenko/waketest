﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class PlayerScoreView : MonoBehaviour
	{
		[SerializeField] private Text coinsCount;

		private const string CoinsFormat = "Coins: {0}";

		private PlayerModel model;

		public void Init(PlayerModel model)
		{
			this.model = model;

			model.CoinsCount.Changed += OnCoinsCountChanged;

			OnCoinsCountChanged(model.CoinsCount.Value);
		}

		public void Deinit()
		{
			model.CoinsCount.Changed -= OnCoinsCountChanged;
		}

		private void OnCoinsCountChanged(int count)
		{
			coinsCount.text = string.Format(CoinsFormat, count);
		}
	}
}