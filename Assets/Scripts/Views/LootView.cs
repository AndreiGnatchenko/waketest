﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class LootView : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private CoinView viewPrefab;

		private List<CoinView> coinViews = new List<CoinView>();

		private BattleModel battleModel;

		public void Init(BattleModel battleModel)
		{
			this.battleModel = battleModel;

			battleModel.LootAdded += OnLootAdded;
			battleModel.LootRemoved += OnLootRemoved;
		}

		public void Deinit()
		{
			battleModel.LootAdded -= OnLootAdded;
			battleModel.LootRemoved -= OnLootRemoved;

			foreach (var view in coinViews)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			coinViews.Clear();
		}

		private void OnLootAdded(LootModel loot)
		{
			var coinView = Instantiate(viewPrefab, viewAnchor);

			coinView.Init(loot);

			coinViews.Add(coinView);
		}

		private void OnLootRemoved(int id)
		{
			var view = coinViews.Find(item => item.Model.Id == id);

			coinViews.Remove(view);

			view.Deinit();

			Destroy(view.gameObject);
		}
	}
}