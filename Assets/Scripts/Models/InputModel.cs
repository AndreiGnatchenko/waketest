﻿using Support;

namespace Game
{
    public class InputModel
    {
        public ReactiveProperty<InputDirection> Movement = new ReactiveProperty<InputDirection>();
    }

    public enum InputDirection
    {
        None,
        Left,
        Right,
        Forward,
        Backward
    }
}