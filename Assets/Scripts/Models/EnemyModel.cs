﻿using System;
using UnityEngine;
using Support;

namespace Game
{
    public class EnemyModel
    {
        public event Action<EnemyModel> Dead;

        public EnemyController Controller { get; set; }
        public ReactiveProperty<Vector3> Position = new ReactiveProperty<Vector3>();
        public ReactiveProperty<Vector3> Direction = new ReactiveProperty<Vector3>();

        public ReactiveProperty<int> Health = new ReactiveProperty<int>();
        public bool IsDead => Health.Value <= 0;

        public int InitialHealth { get; }

        public EnemyModel(int initialHealth)
        {
            InitialHealth = initialHealth;
            Health.Value = initialHealth;
        }

        public void PublishDead(EnemyModel enemy)
        {
            Dead?.Invoke(enemy);
        }
    }
}