﻿using System;
using System.Collections.Generic;

namespace Game
{
	public class BattleModel
	{
		public event Action BattleFinished;
		public event Action<LootModel> LootAdded;
		public event Action<int> LootRemoved;

		public PlayerModel Player { get; }
		public List<EnemyModel> Enemies { get; }
		public List<LootModel> Loot { get; }

		public BattleModel(PlayerModel player, List<EnemyModel> enemies)
		{
			Player = player;
			Enemies = enemies;
			Loot = new List<LootModel>();
		}

		public void PublishLootAdded(LootModel loot)
		{
			LootAdded?.Invoke(loot);
		}

		public void PublishLootRemoved(int id)
		{
			LootRemoved?.Invoke(id);
		}

		public void PublishBattleFinished()
		{
			BattleFinished?.Invoke();
		}
	}
}