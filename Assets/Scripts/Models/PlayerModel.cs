﻿using UnityEngine;
using Support;

namespace Game
{
    public class PlayerModel
    {
        public PlayerController Controller { get; set; }

        public ReactiveProperty<Vector3> Position = new ReactiveProperty<Vector3>();
        public ReactiveProperty<Vector3> Direction = new ReactiveProperty<Vector3>();

        public ReactiveProperty<int> CoinsCount = new ReactiveProperty<int>();
        public ReactiveProperty<bool> Active = new ReactiveProperty<bool>();
        public ReactiveProperty<float> LastFireTime = new ReactiveProperty<float>();

        public PlayerModel()
        {
            LastFireTime.Value = Support.Support.GetTime();
        }
    }
}