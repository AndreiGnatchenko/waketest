﻿using UnityEngine;

namespace Game
{
	public class LootModel
	{
		public int Id { get; }
		public Vector3 Postion { get; }

		public LootModel(int id, Vector3 position)
		{
			Id = id;
			Postion = position;
		}
	}
}