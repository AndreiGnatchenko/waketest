﻿using UnityEngine;

namespace Game
{
	public class BattleWindowModel
	{
		public BattleWindowModel(BattleModel battleModel, Camera battleCamera)
		{
			BattleModel = battleModel;
			BattleCamera = battleCamera;
		}

		public BattleModel BattleModel { get; }
		public Camera BattleCamera { get; }
	}
}